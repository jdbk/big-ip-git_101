# BIG-IP & Git 101

Goal of this lab is to deploy desired configuration to F5 BIGIP from GitLab.
## Lab topology
Following components are used in the lab environment:
  * F5 BIG-IP
    * name: bigip1
    * specs: 4vCPU, 12GB RAM, 120GB HDD
    * modules: LTM, AWAF, DNS
    * AS3 installed
    * vlans: mgmt, v10

  * Applications server (apps running in docker on ubuntu 20.04)
    * name: apps
    * specs: 4vCPU, 8GM RAM, 80GB HDD
    * docker:
      * WebGoat
      * ELK Stack
    * vlans: mgmt, v10
  * GitLab server (ubuntu 20.04)
    * name: gitlab
    * gitlab runner installed and registered
    * specs: 4vCPU, 8GB memory, 80GB HDD
    * vlans: mgmt

Lab diagram: ![lab diagram](/images/bigip-git_lab1.png "lab diagram")

## Lab 1 - basic AS3 deployment
Documentation in [Lab 1](/Lab1-AS3/) folder
## Lab 2 - AS3 & Jinja2 templates
Documentation in [Lab 2](/Lab2-AS3&Templates) folder
## Lab 3 - WAF 
Documentation in [Lab 3](/Lab3 - WAF/) folder
## Lab 4 - WAF sync from BIG-IP to GitLab
Documentation in [Lab 4](/Lab40PolicySync/) folder


## Install notes
### GitLab
#### install gitlab
  * https://about.gitlab.com/install/
  ```bash
  sudo apt-get update
  sudo apt-get install -y curl openssh-server ca-certificates tzdata perl jq
  curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
  ```

  * custom hostname:
  `sudo EXTERNAL_URL="http://gitlab.access.udf.f5.com" apt-get install gitlab-ee`

#### install gitlab runner
  * GitLab -> Menu -> Overview -> Runners -> Register an instance runner
  * Download the binary for your system
`sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`
  * Give it permission to execute
`sudo chmod +x /usr/local/bin/gitlab-runner`
  * Create a GitLab Runner user
`sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`
  * Install and run as a service
`sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner`
`sudo gitlab-runner start`
  * register runner
`sudo gitlab-runner register --url http://localhost/`
  * executor - shell
#### install ansible
  * `sudo apt install -y python3-pip`
  * `sudo python3 -m pip install jinja2==3.1.1`
  * `sudo python3 -m pip install ansible`
  * `ansible-galaxy collection install f5networks.f5_bigip`
  * collection examples: https://github.com/F5Networks/f5-ansible-bigip/tree/main/docs/examples
### Docker backend
#### install docker
  * `curl -fsSL https://get.docker.com -o get-docker.sh`
  * `sudo sh get-docker.sh`
  * `sudo usermod -aG docker $USER`
#### run webgoat
  * `docker run -dp 8080:8080 --restart unless-stopped webgoat/webgoat-8.0`.
#### run ELK
  * `docker run -dp 5601:5601 -p 9200:9200 -p 5044:5044 --restart unless-stopped -it --name elk sebp/elk`
#### run petstore
  * `docker run -dp 8181:8080 --restart unless-stopped  -e SWAGGER_HOST="http://petstore.swagger.io" -e SWAGGER_URL="http://10.1.10.105" -e SWAGGER_BASE_PATH="/v2" swaggerapi/petstore`

## How to replicate at home
  * spin up and configure all the components as described above
  * ssh to GitLab VS
    * `git clone https://gitlab.com/jdbk/big-ip-git_101/`
    * `cd big-ip-git_101`
    * `git remote rm origin`
    * `cd Lab1\ -\ AS3/`
  * log into local GitLab server
    * create new public project - lab1
    * from section "Push an existing folder"copy line starting with `git remote add origin http`
  * back to ssh:
    * `git init`
    * `git config user.email root@local`
    * `git config user.name root`
    * paste copied line, `git remote add origin http://[YOURHOSTNAME]]/[YOURPATH]]/lab1.git`
    * `git add .`
    * `git commit -m "Initial commit"`
    * `git push --set-upstream origin master`
  * repeat for labs 2 and 3, except for the `git config*` commands


